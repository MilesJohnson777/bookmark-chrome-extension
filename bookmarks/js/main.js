//document ready
document.addEventListener('DOMContentLoaded', onLoad);

//listen for form submissions
document.getElementById('myForm').addEventListener('submit', saveBookmark);
document.getElementById('catForm').addEventListener('submit', saveCat);

//listen for filter events
document.getElementById('all-filter').addEventListener('click', () => {
    fetchBookmarks('all');
});
document.getElementById('misc-filter').addEventListener('click', () => {
    fetchBookmarks('Misc');
});
document.getElementById('cat-filter').addEventListener('click', (e) => {
    if(e.target && e.target.nodeName == 'BUTTON' && e.target.dataset.purpose === 'view'){
        fetchBookmarks(e.target.name);
    }
    if(e.target && e.target.nodeName == 'BUTTON' && e.target.dataset.purpose === 'delete'){
        alertModal(e.target.name, e.target.dataset.type);
    }
});

//listen for bookmark result's events
document.getElementById('bookmarksResults').addEventListener('click', (e) => {
    if(e.target && e.target.nodeName == 'BUTTON' && e.target.dataset.purpose == "delete"){
        alertModal(e.target.dataset.cat, e.target.dataset.type, e.target.name, e.target.dataset.blobbybloops);
    }
    if(e.target && e.target.nodeName == 'BUTTON' && e.target.dataset.purpose == "visit"){
        chrome.tabs.create({url: e.target.dataset.blobbybloops});
    }
});

//listen for storage changes
chrome.storage.onChanged.addListener((changes)=>{
    fetchBookmarks(currentCat);
    proagateFilter();
    propagateCats();
});

//global VARS
var currentCat = 'all';
const storageArea = chrome.storage.sync;

//functions to execute upon document ready
function onLoad(){
    fetchBookmarks(currentCat);
    propagateCats();
    proagateFilter();
    structCheck();
}

//check sync storage for data structure / setup default data structure
function structCheck(){
    var bookmarks = [];
    var categories = ['Misc'];
    var librarian = {
        'bookmarks': bookmarks,
        'categories': categories
    }
    storageArea.get(null, (data) => {
        var dataInq = data.bookmarks;
        if(dataInq === undefined){
            storageArea.set(librarian);
        }
    });
}

function saveBookmark(e){
    var siteName = document.getElementById('siteName').value;
    var siteURL = document.getElementById('siteURL').value;
    var siteCat = document.getElementById('siteCat').value;

    //format siteName
    siteName = siteName.charAt(0).toUpperCase() + siteName.slice(1);
    //if no category is selected default to Misc
    if(siteCat == ''){
        siteCat = 'Misc';
    }
    //validate form !null
    if(!siteName || !siteURL){
        alertModal(null, null, 'empty', 'empty', null);
        e.preventDefault();
        return false;
    }

    //regex for URLS
    var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);

    //check that the URL matches the regex pattern
    if(!siteURL.match(regex)){
        alertModal(null, null, null, 'badpattern', null)
        e.preventDefault();
        return false;
    }

    //init bookmark object
    var bookmark = {}

    //check that the URL starts with either http or https
    if(siteURL.indexOf('http://') == 0 || siteURL.indexOf('https://') == 0){
        bookmark = {
            category: siteCat,
            name: siteName,
            url: siteURL
        }
    } else {
        //if not, add https.
        siteURL = 'https://' + siteURL;
        bookmark = {
            category: siteCat,
            name: siteName,
            url: siteURL
        }
    }

    //prevent duplicates
    storageArea.get(null, (data)=> {
        var bookmarks = data.bookmarks;
        var dup;
        for(var i = 0; i < bookmarks.length; i++){
            if(siteName === bookmarks[i].name){
                dup = 'bookDup';
                alertModal(null, null, bookmarks[i].name, null, dup);
                return false;
            }
            if(siteURL === bookmarks[i].url){
                dup = 'urlDup';
                alertModal(null, null, null, bookmarks[i].url, dup);
                return false;
            }
        }
        //save book mark to storage
        var currentBookmarks = data.bookmarks;
        currentBookmarks.push(bookmark);
		storageArea.set({'bookmarks': currentBookmarks});

    });
    document.getElementById('myForm').reset();
    e.preventDefault();
}

//delete specific book mark
function deleteBookmark(url){
    storageArea.get(null, (data)=>{
        var bookmarks = data.bookmarks;
        for(var i = 0; i < bookmarks.length; i++){
            if(bookmarks[i].url == url){
                //maintain current category globally to keep it displayed after deletion
                currentCat = bookmarks[i].category;
                bookmarks.splice(i, 1);
                storageArea.set({'bookmarks': bookmarks});
            }
        }
    });
}

//fetch bookmarks from sync
function fetchBookmarks(cat){
    //update current category
    currentCat = cat;
    displayCat();
    storageArea.get(null, (data)=>{
        var bookmarks = data.bookmarks;
        var bookmarksResults = document.getElementById('bookmarksResults');
        bookmarksResults.innerHTML = '';
        //control whether all categories are displayed or filtered
        if(cat == 'all'){ //all
            for(var i = 0; i < bookmarks.length; i++){
                var name = bookmarks[i].name;
                var url = bookmarks[i].url;
                var categ = bookmarks[i].category;
                catElements(name, url, categ);
            }
        } else { //filtered
            for(var i = 0; i < bookmarks.length; i++){
                if(bookmarks[i].category == cat){
                    var name = bookmarks[i].name;
                    var url = bookmarks[i].url;
                    var categ = bookmarks[i].category;
                    catElements(name, url, categ);
                }
            }
            // if a category has no bookmarks display modified default message
            if(bookmarksResults.innerHTML == ''){
                document.getElementById('bookmarksResults').innerHTML = '<div class="well"><em>No Bookmarks in this category...</em></div>';
            }
        }
        //if there are no bookmarks display default message
        if(bookmarks.length == 0){
            bookmarksResults.innerHTML = '<div class="well"><em>No Bookmarks yet...</em></div>';
        }
    });

}

//write category markup to DOM
function catElements(name, url, categ){
    bookmarksResults.innerHTML += '<div class="well">'+
                                  '<h3>'+name+
                                  ' <button class="btn btn-default" data-purpose="visit" data-blobbybloops="'+url+'">Visit</button>'+
                                  ' <button class="btn btn-danger" data-purpose="delete" data-blobbybloops="'+url+'" data-cat="'+categ+'" name="'+name+'" data-type="bookmark">Delete</button> '+
                                  '</h3>'+
                                  '</div>';
}

// save new category
function saveCat(e){
    var category = document.getElementById('new-cat').value;
    category = category.charAt(0).toUpperCase() + category.slice(1);

    //validate category field has a value
    storageArea.get('categories', (data) => {
        if(!category){
            alertModal('none');
            return false;
        }
        var valCategory = data.categories;
        for(var i = 0; i < valCategory.length; i++){
            if(category === valCategory[i]){
                alertModal(category, null, null, null, 'dupCatDetected');
                return false;
                break;
            }
        }
        data.categories.push(category);
        storageArea.set(data);
    });
    if(document.getElementById('new-cat').value !== null){
        document.getElementById('catForm').reset();
        e.preventDefault();
    }
}

//find all categories and propagate dropdown menu
function propagateCats(){
    var domCats = document.getElementById('siteCat');
    storageArea.get(null, (data)=>{
        var categories = data.categories;
        //alphabetize the array
        categories = categories.sort();
        domCats.innerHTML = '<option value="">select category</option>';
        for(var i = 0; i < categories.length; i++){
            domCats.innerHTML += '<option value="'+categories[i]+'">'+categories[i]+'</option>'
        }
    });
}

//delete an entire category and related bookmarks
function deleteCat(cat){
    storageArea.get(null, (data)=>{
        var bookmarks = data.bookmarks;
        for(var i = bookmarks.length - 1; i >= bookmarks.length || i >= 0; i--){
            if(bookmarks[i].category == cat){
                bookmarks.splice(i, 1);
            }
        }
        storageArea.set({'bookmarks': bookmarks}, ()=>{
            storageArea.get(null, (data)=>{
                var categories = data.categories;
                for(var i = 0; i < categories.length; i++){
                    if(categories[i] == cat){
                        //remove the category from array
                        categories.splice(i, 1);
                        storageArea.set({'categories': categories});
                    }
                }
            });
        });
    });
    //change current category to all by default when deleting
    currentCat = 'all';
    hideModal();
    onLoad();
}

//fill category filter menu
function proagateFilter(){
    var filter = document.getElementById('cat-filter');
    storageArea.get(null, (data)=>{
        var categories = data.categories;
        //alphabetize the array
        categories = categories.sort();
        filter.innerHTML = '';
        for(var i = 0; i < categories.length; i++){
            // to ensure the miscellaneous category never has the option to be deleted
            if(categories[i] === 'Misc'){
                continue;
            } else {
                filter.innerHTML += '<div class="well">'+
                                    '<h5>'+categories[i]+
                                    ' <button class="btn btn-default" data-purpose="view" name="'+categories[i]+'">View</button>'+
                                    ' <button class="btn btn-danger" data-purpose="delete" name="'+categories[i]+'" data-type="category">delete</button>'+
                                    '</h5>'+
                                    '</div>';
            }
        }
    });
}

//create/update category display message
function displayCat(){
    document.getElementById('selected-cat').innerHTML = currentCat;
}

//make modal visible to display message then blur the background
function alertModal(cat, dtype, bname, url, dup){
    var modal = document.getElementsByClassName('modal')[0];
    //change currentCat so deleteCat() can use it for deletion
    currentCat = cat;
    var modalMes = document.getElementById('message');
    modal.style.display = 'block';

    //handle empty form
    if(bname === 'empty' || url === 'empty'){
        modalMes.innerHTML = '<p class="modal-para">Please fill both Site Name and Web Address fields to make an entry.</p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-emptyFormOk-button"> OK </button>'+
                             '</div>';
        document.getElementById('modal-emptyFormOk-button').addEventListener('click', hideModal, {once: true});
    }
    if(url === 'badpattern'){
        modalMes.innerHTML = '<p class="modal-para">The Web Address you entered is not valid.</p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-badRegexOk-button"> OK </button>'+
                             '</div>';
        document.getElementById('modal-badRegexOk-button').addEventListener('click', hideModal, {once: true});
    }

    //handle category form duplicates
    if(cat === 'none'){
        modalMes.innerHTML = '<p class="modal-para">Category entry field cannot be blank.</p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-catOk-button"> OK </button>'+
                             '</div>';
        document.getElementById('modal-catOk-button').addEventListener('click', hideModal, {once: true});
    }
    if(dup === 'dupCatDetected'){
        modalMes.innerHTML = '<p class="modal-para">You already have a category titled "'+cat+'".</p>'+
                             '<p class="modal-para">Please select another category title.</p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-catOk-button"> OK </button>'+
                             '</div>';
        document.getElementById('modal-catOk-button').addEventListener('click', hideModal, {once: true});
    }

    //handle bookmark form duplicates
    if(dup === 'bookDup'){
        modalMes.innerHTML = '<p class="modal-para">A bookmark named "'+bname+'" already existed.</p>'+
                             '<p class="modal-para">Please choose another name.</p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-bookOk-button"> OK </button>'+
                             '</div>';
        document.getElementById('modal-bookOk-button').addEventListener('click', hideModal, {once: true});
    }
    if(dup === 'urlDup'){
        modalMes.innerHTML = '<p class="modal-para">The following web address is already bookmarked:</p>'+
                             '<p class="modal-para">'+url+'</p>'+
                             '<p class="modal-para">Please choose another.</p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-urlOk-button"> OK </button>'+
                             '</div>';
        document.getElementById('modal-urlOk-button').addEventListener('click', hideModal, {once: true});
    }

    //determine if bookmark or category deletion is desired
    if(dtype === 'category'){
        var bCounter = 0;
        storageArea.get(null, (data)=>{
            var bookmarks = data.bookmarks;
            for(var i = 0; i < bookmarks.length; i++){
                if(bookmarks[i].category == cat){
                    bCounter++;
                }
            }
            modalMes.innerHTML = '<p class="modal-para">You are about to delete the entire category "'+cat+'" and all of its related bookmarks.</p>'+
                                 '<p class="modal-para">This will erase <strong>'+bCounter+'</strong> bookmarks.'+
                                 '<p class="modal-para">Are you sure?<p>'+
                                 '<div>'+
                                 '<button class="btn btn-default" id="modal-cancel-button">cancel</button>'+
                                 '<button class="btn btn-danger" id="delete-cat-button">delete</button>'+
                                 '</div>';
            bCounter = 0;
            document.getElementById('modal-cancel-button').addEventListener('click', hideModal, {once: true});
            document.getElementById('delete-cat-button').addEventListener('click', ()=>{
                deleteCat(currentCat);
                hideModal();
            });
        });
    }
    if(dtype === 'bookmark'){
        modalMes.innerHTML = '<p class="modal-para">You are about to delete the bookmark for '+bname+'.</p>'+
                             '<p class="modal-para">Are you sure?<p>'+
                             '<div>'+
                             '<button class="btn btn-default" id="modal-cancel-button">cancel</button>'+
                             '<button class="btn btn-danger" id="delete-book-button">delete</button>'+
                             '</div>';
        document.getElementById('modal-cancel-button').addEventListener('click', hideModal, {once: true});
        document.getElementById('delete-book-button').addEventListener('click', ()=>{
            deleteBookmark(url);
            hideModal();
        });
    }
    document.getElementsByClassName('blur')[0].style.filter = 'blur(5px)';
}

//hide the modal and refocus the background
function hideModal(){
    var modal = document.getElementsByClassName('modal')[0];
    modal.style.display = 'none';
    document.getElementsByClassName('blur')[0].style.filter = 'blur(0px)';
}

//alternative to clicking cancel should the user want the modal
//to vanish by clicking outside the box
window.onclick = function(e){
    var modal = document.getElementsByClassName('modal')[0];
    if(e.target == modal){
        hideModal();
    }
}
