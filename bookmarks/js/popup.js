document.addEventListener('DOMContentLoaded', ()=>{
    structCheck();

    //listen for quick save button
    document.querySelector('#quick-save').addEventListener('click', ()=>{
        var queryInfo = {
            active: true,
            currentWindow: true
        };
        chrome.tabs.query(queryInfo, (tabs) => {
            var tab = tabs[0];
            var url = tab.url;
            var title = tab.title;
            //console.assert(typeof url == 'string');
            //console.assert(typeof title == 'string');
            //init bookmark object
            var bookmark = {}
            //check that the URL starts with either http or https
            if(url.indexOf('http://') == 0 || url.indexOf('https://') == 0){
                bookmark = {
                    category: 'Misc',
                    name: title,
                    url: url
                }
            } else {
                //if not, add https.
                url = 'https://' + url;
                bookmark = {
                    category: 'Misc',
                    name: title,
                    url: url
                }
            }
            //check for duplicates and save
            chrome.storage.sync.get(null, (data)=>{
                var bookmarks = data.bookmarks;
                for(var i = 0; i < bookmarks.length; i++){
                    if(bookmarks[i].url == bookmark.url){
                        failOrSuccess(false);
                        return false;
                    }
                }
                bookmarks.push(bookmark);
                chrome.storage.sync.set({'bookmarks': bookmarks}, ()=>{
                    failOrSuccess(true);
                });
            });
        });
    });

    // ensure setup of sync data structure
    function structCheck(){
        var bookmarks = [];
        var categories = ['Misc'];
        var librarian = {
            'bookmarks': bookmarks,
            'categories': categories
        }
        chrome.storage.sync.get(null, (data) => {
            var dataInq = data.bookmarks;
            if(dataInq === undefined){
                chrome.storage.sync.set(librarian);
            }
        });
    }

    //display confirmation to the user
    function failOrSuccess(failPass){
        if(failPass === true){
            document.getElementById('modal-content').style.display = 'none';
            document.getElementById('saved').style.display = 'block';
            setTimeout(window.close, 2000);
        } else {
            document.getElementById('modal-content').style.display = 'none';
            document.getElementById('rejected').style.display = 'block';
            setTimeout(window.close, 2000);
        }
    }

});
